import time
from ewokscore.task import Task


class WaiterTask(
    Task,
    input_names=("object", "seconds"),
    output_names=("object",),
):
    """
    Wait for n seconds and return the provided object.
    """

    def run(self):
        time.sleep(self.inputs.seconds)
        self.outputs.object = self.inputs.object
