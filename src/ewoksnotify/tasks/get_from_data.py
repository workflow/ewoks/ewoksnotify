from ewokscore.task import Task


class GetFromDataTask(
    Task,
    input_names=("data", "key_or_index"),
    output_names=("element",),
):
    """
    Extract a value from a dictionary or list.
    """

    def run(self):
        input_data = self.inputs.data
        input_key_or_index = self.inputs.key_or_index

        # Check data type
        if isinstance(input_data, list):
            # Extract element from a list
            # Check if the index is out of range
            if isinstance(input_key_or_index, int) and input_key_or_index < len(
                input_data
            ):
                self.outputs.element = input_data[input_key_or_index]
            else:
                raise IndexError("Error: The index is out of range or not an integer!")

        elif isinstance(input_data, dict):
            # Extract element from a dict
            # Check if the key is in the dictonary
            if input_key_or_index in input_data:
                self.outputs.element = input_data[input_key_or_index]
            else:
                raise KeyError("Error: Key not found in the dictonary")

        else:
            raise TypeError("Error: The data should be a list or a dict!")
