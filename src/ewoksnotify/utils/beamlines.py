ALL_BEAMLINES = tuple(
    sorted(
        (
            "id01",
            "id02",
            "id03",
            "id06",
            "id09",
            "id10",
            "id11",
            "id12",
            "id13",
            "id15a",
            "id15b",
            "id16a",
            "id16b",
            "id17",
            "id18",
            "id19",
            "id20",
            "id21",
            "id22",
            "id23-1",
            "id23-2",
            "id24",
            "id26",
            "id27",
            "id28",
            "id29",
            "id30a-1",
            "id30a-2",
            "id30a-3",
            "id30b",
            "id31",
            "id32",
            "bm01",
            "bm02",
            "bm05",
            "bm07",
            "bm08",
            "bm014",
            "bm016",
            "bm018",
            "bm020",
            "bm023",
            "bm025",
            "bm026",
            "bm028",
            "bm029",
            "bm030",
            "bm031",
            "bm032",
            "cm01",
        )
    )
)
