.. 
    this file is used to provide a reference page for orange-help system. As elsewhere we are using pydata_sphinx_theme with possibly some advance widget here we ensure to keep a flat / simple layout of the html page

Widgets
"""""""

.. toctree::
  :maxdepth: 1

  tone_notifier.rst
  waiter.rst
