Canvas
------


.. toctree::
  :maxdepth: 1
  :hidden:

  widgets/tone_notifier.rst
  widgets/waiter.rst


.. grid:: 5

   .. grid-item-card::
      :img-top: widgets/img/icons/tone_notifier.svg

      :doc:`widgets/tone_notifier`

   .. grid-item-card::
      :img-top: widgets/img/icons/waiter.png

      :doc:`widgets/waiter`