ewoksnotify |version|
=====================

ewoksnotify is a common ewoks project containing tasks that can be of a general usage for 'notification'


Getting started
---------------

Install the project

.. code-block:: bash

    pip install ewoksnotify[full]

Documentation
-------------

.. toctree::
    :maxdepth: 2
    :hidden:

    canvas/index
    api
